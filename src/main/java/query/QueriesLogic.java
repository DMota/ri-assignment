package query;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.xml.soap.Text;

import auxiliary.Data;
import rankedretrieval.RankedLogic;
import rankedretrieval.RankedRetrieval;
import tokenizer.ImprovedTokenizer;

/**
 * 
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class QueriesLogic {
	
	private ImprovedTokenizer iTok = new ImprovedTokenizer();

	/**
	 * Construtor
	 */
	public QueriesLogic() {

	}

	/**
	 * 
	 * @param index
	 * @param index2
	 * @param q
	 * @param ranking
	 * @param vec
	 * @param queries
	 * @throws IOException 
	 * @throws NumberFormatException 
	 */
	public void procQ(HashMap<String, Data> index, HashMap<String, Double> index2, String q, HashMap<Integer, RankedRetrieval> ranking, LinkedHashMap<Integer, ArrayList<String>> queries) throws NumberFormatException, IOException {
		//System.out.println("Entrei logica queries.");
		FileReader fr = new FileReader(q);
		BufferedReader br = new BufferedReader(fr);
		String line;
		int idQuery = 1;
		while ((line = br.readLine()) != null) {
			StringBuilder holder = iTok.improvedToken(line);
			ArrayList<String> text = new ArrayList<>();
			text.add(holder.toString());
			RankedRetrieval rank = new RankedRetrieval();
			Collection<String> list = new ArrayList<String>();
			list.add(text.get(0));
			for (int i = 1; i < text.size(); i++){
				list.add(text.get(i));
			}
			text.addAll(list);
			list.clear();
			queries.put(idQuery, text);		
			rank.logicaRank(index, text, index2);		
			ranking.put(idQuery, rank);
			idQuery += 1;
		}

	}

}
