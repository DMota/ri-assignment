package query;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import auxiliary.Data;
import rankedretrieval.RankedRetrieval;

/**
 * 
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class Queries {

	private HashMap<String, Data> index;
	private HashMap<String, Double> index2;
	private HashMap<Integer, RankedRetrieval> rank;
	private LinkedHashMap<Integer, ArrayList<String>> queriesList;
	
	private static QueriesLogic qLogic = new QueriesLogic();
	
	private static final File ROOT = new File("files");
	private static final String BARRA = System.getProperty("file.separator");
	private static final String QUERIES = ROOT + BARRA + "cranfield.queries.txt";
	
	/**
	 * Construtor
	 */
	public Queries() {
		this.index = new HashMap<>();
		this.index2 = new HashMap<>();
		this.rank = new HashMap<>();
		this.queriesList = new LinkedHashMap<>();
	}
	
	/**
	 * 
	 * @throws IOException
	 */
	public void processarQueries() throws IOException{
		qLogic.procQ(index, index2, QUERIES, rank, queriesList);
	}

	public double size() {		
		return rank.size();
	}
}
