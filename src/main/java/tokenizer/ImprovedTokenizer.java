package tokenizer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import org.tartarus.snowball.ext.englishStemmer;

import corpusreader.ReadFile;
import corpusreader.WriteFile; 

/** 
 * Classe do improved tokenizer
 * Aplica filtros necessarios e escreve ficheiro so com tokens
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class ImprovedTokenizer {

	protected static ArrayList<String> stopWords = new ArrayList<>();

	private static final File ROOT = new File("files");
	private static final String BARRA = System.getProperty("file.separator");
	private static final String STOP = ROOT + BARRA + "stop.txt";

	private static final String TOKENIZER = ROOT + BARRA + "_tok.tsv";

	private static SimpleTokenizer sTok = new SimpleTokenizer();

	/**
	 * Construtor
	 */
	void improvedTokenizer() {

	}

	/**
	 * Le stopwords do documento txt do path recebido 
	 * @param path
	 * @throws FileNotFoundException
	 */
	public static void lerStopWords() throws FileNotFoundException {
		Scanner sc = new Scanner(new File(STOP));

		//guardar stopwords no arraylist
		while (sc.hasNextLine()) {
			stopWords.add(sc.nextLine());
		}

		sc.close();
	}

	/**
	 * Remove stopwords do texto
	 * Stemm do texto
	 * @param s
	 * @return 
	 * @throws IOException 
	 */

	public StringBuilder improvedToken(String s) throws IOException{
		try {
			lerStopWords();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		StringBuilder sb = new StringBuilder();
		//String texto = "", textoConcat = "";

		String[] split_line = s.split("\n");
		String[] split_criteria;//, split_tab;;
		for(String i: split_line){
			split_criteria = i.split("\\s+");
			for (String tab : split_criteria){
				//split_criteria = tab.split(" ");
				//for(String s2: split_criteria){
				
				if (!(stopWords.contains(tab) || tab.equals(" "))){
					englishStemmer stemmer = new englishStemmer();
					stemmer.setCurrent(tab);
					stemmer.stem();
					//				text.add(stemmer.getCurrent());
					//                                textoConcat = texto.concat(stemmer.getCurrent()+" ");
					sb.append(sTok.simpleToken(stemmer.getCurrent())).append(" ");
				}
				else{
					sb.append(sTok.simpleToken(tab).toString()).append(" ");
				}
			}
			//sb.append("\t");
			//}
			//                    textoConcat.concat("\n");
			sb.append("\n");
		}
/*
		//                split_line = textoConcat.split("\n");
		split_line = sb.toString().split("\n");
		//                System.out.println(split_line);
		sb.setLength(0);
		StringBuilder sb2 = new StringBuilder();
		for(String i: split_line){
			//split_tab = i.split("\t");
			//for (String tab : split_tab){
			//                    for(String s3 : text){
			split_criteria = i.split(" ");
			for(String s3 : split_criteria){
				String s4 = sTok.simpleToken(s3).toString();
				sb2.append(s4).append(" ");
				//                            sb.append(" ");
			}
			//sb2.append("\t");
			//}
			sb2.append("\n");
		}

		//                System.out.println(sb);

		//		File tokFile = new File(TOKENIZER);
		//		FileOutputStream fos = new FileOutputStream(tokFile);
		//		fos.write(sb.toString().getBytes());
		 * 
		 */
		return sb;
	}
}
