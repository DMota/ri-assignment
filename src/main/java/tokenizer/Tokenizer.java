package tokenizer;

import Assignment3.HeapMemory;
import corpusreader.ReadFile;
import corpusreader.WriteFile;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/** 
 * Classe tokenizer
 * Da o ficheiro para ser tokenizado
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class Tokenizer {

	private static final File ROOT = new File("files");
	private static final String BARRA = System.getProperty("file.separator");
	private static final String CORPUS = ROOT + BARRA + "_XMLcorpus.tsv";
	private static final String TOKENIZER = ROOT + BARRA + "_tok.tsv";

	//private SimpleTokenizer sTok = new SimpleTokenizer();
	private ImprovedTokenizer iTok = new ImprovedTokenizer();

	/**
	 * Construtor
	 */
	void tokenizer() {

	}

	/**
	 * Chama simpletokenizer e improvedtokenizer
	 * @param s 
	 * @return 
	 * @throws IOException 
	 */
	public StringBuilder tokenization(StringBuilder texto) throws IOException{

		//sTok.simpleToken(sb);
		//iTok.improvedToken(sb);

		//        ReadFile rf = new ReadFile();
		//        BufferedReader br = new BufferedReader();
		//        br = rf.readFile(File "_new.tsv");


		return iTok.improvedToken(texto.toString());
	}

	public void tokenize() throws FileNotFoundException, IOException{
		HeapMemory hm = new HeapMemory();
		File ficheiro = new File(CORPUS);
		ReadFile rf = new corpusreader.ReadFile();
		BufferedReader br = new BufferedReader(rf.readFile(ficheiro));

		WriteFile wf = new WriteFile(TOKENIZER);
		StringBuilder sb = new StringBuilder();
		String texto;
		while ((texto = br.readLine()) != null){
			sb.append(texto).append("\n");
			if (hm.getUsedHeapPercentage()>50d) { 
				//System.out.println(hm.setDecimalFormat(hm.getUsedHeapPercentage())+"%");
				//                tok.tokenization(sb);
				//                wf.fileBodyWrite(sb);
				wf.fileBodyWrite(tokenization(sb));
				//System.out.println("Saí do tokenizer");
				sb.setLength(0);
				hm.freeHeapMemory();
			}
		}

		wf.fileBodyWrite(tokenization(sb));
		wf.closeFile();
	}

}