package tokenizer;

import java.io.File;
import java.util.ArrayList;
import java.util.StringTokenizer;

/** 
 * Classe SimpleTokenizer
 * Divide pelo espaco, tira strings pequenas, deixa tudo em miniscula e so com letras
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class SimpleTokenizer {
	
	/**
	 * Construtor
	 */
	void simpleTokenizer (){
		
	}

	/**
	 * divide pelo espaco, tira strings pequenas, 
	 * deixa tudo em miniscula e so com letras
	 * @param s
	 * @return
	 */
	protected StringBuilder simpleToken(String s){
		//removes all nonalphabetic characters
		//non alphanumeric [^A-Za-z0-9]???
		s = s.replaceAll("[^a-zA-Z]", "");
		
		//keep terms with 3 or more characters
		s = s.replaceAll("\\b\\w{1,2}\\b\\s?", "");
		
		//only one space
		//s = s.replaceAll(" +", " ");
		
		//to lower case
		s = s.toLowerCase();
		
		StringBuilder sb = new StringBuilder();
		StringTokenizer st = new StringTokenizer(s);
	
		//splits by whitespace
		while (st.hasMoreElements()) {
			sb.append(st.nextToken());
		}
		
		return sb;
	}
}
