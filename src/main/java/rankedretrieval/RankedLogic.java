package rankedretrieval;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

import Assignment3.HeapMemory;
import auxiliary.Data;
import auxiliary.Mapa;

/**
 * 
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class RankedLogic {

	private static final File ROOT = new File("files");
	private static final String BARRA = System.getProperty("file.separator");
	private static final String RANKED = ROOT + BARRA + "_ranked.tsv";

	private Mapa map = new Mapa();

	/**
	 * 
	 * @param index
	 * @param index2
	 * @param score
	 * @param file
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public void logicaRank(HashMap<String, Data> index, HashMap<String, Double> index2, HashMap<Integer, Double> score, String file) throws NumberFormatException, IOException {	
		//System.out.println("Entrei logica ranked retrieval.");
		HeapMemory hm = new HeapMemory();
		PrintWriter writer = new PrintWriter(RANKED, "UTF-8");

		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line;
		String[] text = null;
		//String[] values = null;

		while ((line = br.readLine()) != null) {
			HashMap<String, Double> temp = new HashMap<>();
			text = line.split("\\s+");
			map.readIndex(index, index2, line);
			map.rankIndex(index, index2, score, text, temp);
			if (hm.getUsedHeapPercentage()>60d) { 
				//System.out.println(hm.setDecimalFormat(hm.getUsedHeapPercentage())+"%");
				map.writeIndex(index, writer);
				temp.clear();
				index.clear();
				hm.freeHeapMemory();
			}
		}
		map.writeIndex(index, writer);
		writer.close();
	}

	/**
	 * 
	 * @param index3
	 * @param index4
	 * @param score
	 * @param text
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public void logicaRank2(HashMap<String, Data> index3, HashMap<String, Double> index4, HashMap<Integer, Double> score, ArrayList<String> text) throws NumberFormatException, IOException {
		HeapMemory hm = new HeapMemory();
		PrintWriter writer = new PrintWriter(RANKED, "UTF-8");

		//String line;
		String[] texto = null;
		//String[] values = null;

		for (String line: text) {
			HashMap<String, Double> temp = new HashMap<>();
			texto = line.split("\\s+");
			map.readIndex(index3, index4, line);
			map.rankIndex(index3, index4, score, texto, temp);
			if (hm.getUsedHeapPercentage()>60d) { 
				//System.out.println(hm.setDecimalFormat(hm.getUsedHeapPercentage())+"%");
				map.writeIndex(index3, writer);
				temp.clear();
				index3.clear();
				hm.freeHeapMemory();
			}
		}
		map.writeIndex(index3, writer);
		writer.close();
		
	}

}