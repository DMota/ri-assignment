package rankedretrieval;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import auxiliary.Data;

/**
 * 
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class RankedRetrieval {
	
	private static RankedLogic rLogic = new RankedLogic();
	
	private static final File ROOT = new File("files");
	private static final String BARRA = System.getProperty("file.separator");
	private static final String EXTINDEXER = ROOT + BARRA + "_extindex.tsv";
	
	private HashMap<String, Data> index;
	private HashMap<String, Double> index2;
	private HashMap<Integer, Double> score;

    public RankedRetrieval() {
    	this.index = new HashMap<>();
		this.index2 = new HashMap<>();
        this.score = new HashMap<>();
    }

    public HashMap<Integer, Double> getScore() {
        return score;
    }
 
    public void rankRet() throws IOException{
		rLogic.logicaRank(index, index2, score, EXTINDEXER);
	}

	public void logicaRank(HashMap<String, Data> index3, ArrayList<String> text, HashMap<String, Double> index4) throws NumberFormatException, IOException {
		rLogic.logicaRank2(index3, index4, score, text);		
	}

}
