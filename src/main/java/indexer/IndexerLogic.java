package indexer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

import Assignment3.HeapMemory;
import auxiliary.Data;
import auxiliary.Mapa;
import tokenizer.ImprovedTokenizer;

/**
 * 
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class IndexerLogic {

	private Mapa map = new Mapa();

	private static final File ROOT = new File("files");
	private static final String BARRA = System.getProperty("file.separator");
	private static final String INDEXER = ROOT + BARRA + "_index.tsv";

	/**
	 * Construtor
	 */
	public IndexerLogic() {

	}

	/**
	 * 
	 * @param index
	 * @param index2
	 * @param file
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public void logicaIndex(HashMap<String, Data> index, HashMap<String, Double> index2, String file) throws NumberFormatException, IOException{
		//System.out.println("Entrei logica index.");
		HeapMemory hm = new HeapMemory();
		PrintWriter writer = new PrintWriter(INDEXER, "UTF-8");
		BufferedReader br = corpusreader.ReadFile.readFile(new File(file));
		String[] text;
		int i = 0;
		String line = null;
		while ((line = br.readLine()) != null) {
			HashMap<String, Data> temp = new HashMap<>();
			text = line.split("\\s+");
			map.tempIndex(temp, i, text);
			map.fillIndex(index, temp, file);	
			i++;
			if (hm.getUsedHeapPercentage()>60d) { 
				//System.out.println(hm.setDecimalFormat(hm.getUsedHeapPercentage())+"%");
				map.writeIndex(index, writer);
				temp.clear();
				index.clear();
				hm.freeHeapMemory();
			}
		}
		map.writeIndex(index, writer);
		writer.close();
	}

}
