package indexer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import auxiliary.Data;
import tokenizer.SimpleTokenizer;

/**
 *  
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class Indexer {

	private HashMap<String, Data> index;
	private HashMap<String, Double> index2;
	
	private static IndexerLogic iLogic = new IndexerLogic();
	private static ExtendIndexer eIndex = new ExtendIndexer();
	
	private static final File ROOT = new File("files");
	private static final String BARRA = System.getProperty("file.separator");
	private static final String TOKENIZER = ROOT + BARRA + "_tok.tsv";
	
	/**
	 * Construtor
	 */
	public Indexer() {
		this.index = new HashMap<>();
		this.index2 = new HashMap<>();
	}
	
	/**
	 * 
	 * @param index Index 
	 * @param index2 Data do Index
	 * @param TOKENIZER Ficheiro tokenizer
	 * @throws IOException
	 */
	public void indexar() throws IOException{
		iLogic.logicaIndex(index, index2, TOKENIZER);
	}
	
	public void extendIndexar() throws NumberFormatException, IOException {
		eIndex.logicaExtendIndex(index, index2, TOKENIZER);
	}
	
}
