package auxiliary;

import java.util.HashMap;

/**
 * 
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class Data {

	//private HashMap<String, Data> index;
	//private HashMap<String, Double> index2;

	private HashMap<Integer, Double> info;

	public Data() {
		//this.index = new HashMap<>();
		//this.index2 = new HashMap<>();
		this.info = new HashMap<>();
	}

	/*public void addindex2(String id) {
        if (index2.containsKey(id)) {
            index2.put(id, index2.get(id) + 1);
        } else {
            index2.put(id, 1.0);
        }
    }

    public HashMap<String, Data> getIndex() {
		return index;
	}

    public HashMap<String, Double> getIndex2() {
        return index2;
    }*/

	public void addInfo(int id) {
		if (info.containsKey(id)) {
			info.put(id, info.get(id) + 1);
		} else {
			info.put(id, 1.0);
		}
	}
	
	public HashMap<Integer, Double> getInfo() {
		return info;
	}

}
