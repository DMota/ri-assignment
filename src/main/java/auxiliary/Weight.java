package auxiliary;

import java.util.HashMap;

/**
 * 
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class Weight {
	
	public void calcIDF(HashMap<String, Data> index, HashMap<String, Double> index2, int totalDocumentos){
		for (HashMap.Entry<String,Data> entry : index.entrySet()){
            index2.put(entry.getKey(),Math.log10(totalDocumentos/index2.get(entry.getKey())));
        }
	}

}
