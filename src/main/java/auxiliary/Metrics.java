package auxiliary;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import rankedretrieval.RankedRetrieval;
import relevance.RelevanceLogic;

/**
 * 
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class Metrics {

	private static final File ROOT = new File("files");
	private static final String BARRA = System.getProperty("file.separator");
	private static final String RELEVANCE = ROOT + BARRA + "cranfield.query.relevance.txt";

	private static RelevanceLogic rLogic = new RelevanceLogic();

	private HashMap<Integer, auxiliary.Data> index;
	private double precision;
	private double recall;
	private double fmeasure;
	private double avgPrecision;
	private double precisionRank10;
	private double ndcg; //IR6-evaluation pagina 43

	/**
	 * Construtor
	 * @param index
	 * @param f
	 * @throws IOException 
	 */
	public Metrics(HashMap<Integer, Data> index) throws IOException {
		this.index = index;
		precision = 0;
		recall = 0;
		fmeasure = 0;
		avgPrecision = 0;
		precisionRank10 = 0;
		ndcg = 0;
		calculateMeasures();
		calculateNDCG();
	}

	/**
	 * Get mean precision
	 * @return precision
	 */
	public double getMeanPrecision() {
		return precision;
	}

	/**
	 * Get mean recall
	 * @return recall
	 */
	public double getMeanRecall() {
		return recall;
	}

	/**
	 * Get mean fmeasure
	 * @return fmeasure
	 */
	public double getMeanFMeasure() {
		return fmeasure;
	}

	/**
	 * Get mean average precision
	 * @return avgPrecision
	 */
	public double getMeanAveragePrecision() {
		return avgPrecision;
	}

	/**
	 * Get mean average precision at rank 10
	 * @return precisionRank10
	 */
	public double getMeanPrecisionAtRank10() {
		return precisionRank10;
	}

	/**
	 * Get normalized discounted cumulative gain 
	 * @return ndcg
	 */
	public double getNDCG(){
		return ndcg;
	}
	
	
	/**
	 * 
	 * @throws IOException
	 */
	private void calculateMeasures() throws IOException {
		for (Entry<Integer, Data> query: rLogic.readRel(index, RELEVANCE).entrySet()) {
			int queryId = query.getKey();

			HashMap<Integer, Double> fDocuments = query.getValue().getInfo();
			HashMap<Integer, Double> indexDocuments = index.get(queryId).getInfo();
			HashMap<Integer, Double> tenFirstDocuments = getTenFirst(indexDocuments);

			int nRelevantsFound = nRelevant(fDocuments, indexDocuments).size();
			int nRelevantsFoundInTen = nRelevant(fDocuments, tenFirstDocuments).size();

			double queryPrecision = (double) nRelevantsFound / indexDocuments.size();
			double queryPrecision10 = (double) nRelevantsFoundInTen / indexDocuments.size();
			double queryRecall = (double) nRelevantsFound / fDocuments.size();

			precision += queryPrecision;
			precisionRank10 += queryPrecision10;
			recall += queryRecall;
			fmeasure += (2 * queryRecall * queryPrecision) / (queryRecall + queryPrecision);
		}

		precision = precision / index.size();
		precisionRank10 = precisionRank10 / index.size();
		recall = recall / index.size();
		fmeasure = fmeasure / index.size();
	}

	/**
	 * 
	 * @param indexDocuments
	 * @return
	 */
	private HashMap<Integer, Double> getTenFirst(HashMap<Integer, Double> indexDocuments) {
		return (HashMap<Integer, Double>) indexDocuments.entrySet().stream().limit(10).collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
	}

	/**
	 * 
	 * @param fDocuments
	 * @param indexDocuments
	 * @return
	 */
	private Map<Integer, Double> nRelevant(Map<Integer, Double> fDocuments, Map<Integer, Double> indexDocuments) {
		return fDocuments.entrySet().stream().filter(value -> indexDocuments.containsKey(value.getKey())).collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
	}

	/**
	 * 
	 * @throws IOException
	 */
	private void calculateNDCG() throws IOException {
		for (Map.Entry<Integer, Data> query : rLogic.readRel(index, RELEVANCE).entrySet()) {
			int queryId = query.getKey();

			Map<Integer, Double> fDocuments = query.getValue().getInfo();
			Map<Integer, Double> indexDocuments = index.get(queryId).getInfo();
			Map<Integer, Double> relevantDocuments = nRelevant(fDocuments, indexDocuments);

			int docsRead = 0, relevantDocsRead = 0;
			double queryPrecision = 0, index2 = 0;
			boolean foundDocument = false;

			for (Map.Entry<Integer, Double> document : indexDocuments.entrySet()) {        
				docsRead++;
				index2++;

				if (relevantDocuments.containsKey(document.getKey())) {
					relevantDocsRead++;
					queryPrecision += (double) relevantDocsRead / docsRead;
					if (!foundDocument) {
						ndcg += 1 / index2;
						foundDocument = true;
					}
				}
			}
			avgPrecision += queryPrecision / relevantDocuments.size();
		}
		avgPrecision = avgPrecision / index.size();
		ndcg = ndcg / index.size();
	}
}
