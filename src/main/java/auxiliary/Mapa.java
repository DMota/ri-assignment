package auxiliary;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import auxiliary.Data;

/**
 * Classe Mapa
 * Metodos do hashmap do index
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class Mapa {

	/**
	 * Le o ficheiro com o hashmap extended
	 * Preenche o hashmap index e index2
	 * @param index Index
	 * @param index2 Data do Index
	 * @param file Ficheiro tokenizer
	 * @return 
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public void readIndex(HashMap<String, Data> index, HashMap<String, Double> index2, String text) throws NumberFormatException, IOException{
		String[] t = text.split(",");
		Data data = new Data();

		for (int i = 1; i < t.length; i++) {
			String[] values = t[i].split(":");
			data.getInfo().put(Integer.parseInt(values[0]), Double.parseDouble(values[1]));
		}

		index2.put(t[0], (double)t.length-1);
		index.put(t[0], data);
	}


	/**
	 * 
	 * @param index
	 * @param temp
	 * @param file
	 * @throws FileNotFoundException
	 */
	public void fillIndex(HashMap<String, Data> index, HashMap<String, Data> temp, String file) throws FileNotFoundException {
		for (HashMap.Entry<String, Data> entrada : temp.entrySet()) {
			for (HashMap.Entry<Integer, Double> entradas : entrada.getValue().getInfo().entrySet()) {
				if (!index.containsKey(entrada.getKey())) {
					Data data = new Data();
					data.getInfo().put(entradas.getKey(), entradas.getValue());
					index.put(entrada.getKey(), data);
				} else {
					index.get(entrada.getKey()).getInfo().put(entradas.getKey(), entradas.getValue());
				}
			}
		}
	}

	/**
	 * Percorre o hashmap temporario e preenche o do index2
	 * Calcula o peso do termo e retorna-o
	 * @param index2
	 * @param temp hashmap temporario
	 * @return sum, peso do termo
	 */
	public double getSum(HashMap<String, Double> index2, HashMap<String, Data> temp) {
		double sum = 0;

		for (HashMap.Entry<String, Data> entrada : temp.entrySet()) {
			for (HashMap.Entry<Integer, Double> entradas : entrada.getValue().getInfo().entrySet()) {
				entrada.getValue().getInfo().put(entradas.getKey(), (1 + Math.log10(entradas.getValue())));
				sum += entradas.getValue() * entradas.getValue();
			}

			if(!index2.containsKey(entrada.getKey())){
				index2.put(entrada.getKey(),1.0);
			}
			else{
				index2.put(entrada.getKey(),index2.get(entrada.getKey()) + 1.0);
			}
		}
		sum = Math.sqrt(sum);		
		return sum;
	}

	/**
	 * Preenche extended index atraves do temporario
	 * @param index hashmap a ser preenchido
	 * @param temp hashmap temporario
	 * @param file
	 * @param sum peso do termo
	 */
	public void fillExtIndex(HashMap<String, Data> index, HashMap<String, Data> temp, String file, Double sum) {
		for (HashMap.Entry<String, Data> entrada : temp.entrySet()) {
			for (HashMap.Entry<Integer, Double> entradas : entrada.getValue().getInfo().entrySet()) {
				if (!index.containsKey(entrada.getKey())) {
					Data data = new Data();
					data.getInfo().put(entradas.getKey(), Math.round((entradas.getValue() / sum ) * 100.0) / 100.0);
					index.put(entrada.getKey(), data);
				} else {
					index.get(entrada.getKey()).getInfo().put(entradas.getKey(), Math.round((entradas.getValue() / sum ) * 100.0) / 100.0);
				}
			}
		}
	}

	/**
	 * Escreve o hashmap index
	 * @param index hashmap a ser escrito
	 * @param writer
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public void writeIndex(HashMap<String, Data> index, PrintWriter writer) throws FileNotFoundException, UnsupportedEncodingException{
		for (HashMap.Entry<String, Data> entrada : index.entrySet()) {
			if (entrada.getKey().equals("")) {
				continue;
			}
			writer.print(entrada.getKey());
			HashMap<Integer, Double> data = entrada.getValue().getInfo();

			for (Entry<Integer, Double> entradas : data.entrySet()) {
				writer.print("," + entradas.getKey() + ":" + entradas.getValue());
			}
			writer.println();
		}
	}

	/**
	 * Preenche o hashmap temporario
	 * @param temp hashmap temporario
	 * @param i
	 * @param text 
	 */
	public void tempIndex(HashMap<String, Data> temp, int i, String[] text) {
		for (int j = 0; j < text.length; j++) {
			String textaux = text[j];
			if (!temp.containsKey(textaux)) {
				Data data = new Data();
				data.addInfo(i + 1);
				temp.put(textaux, data);
			} else {
				temp.get(textaux).addInfo(i + 1);
			}
		}
	}

	/**
	 * 
	 * @param index
	 * @param index2
	 * @param score
	 * @param text
	 */
	public void rankIndex(HashMap<String, Data> index, HashMap<String, Double> index2, HashMap<Integer, Double> score, String[] text, HashMap<String, Double> temp) {

		for (String text1 : text) {
			if (!temp.containsKey(text1)) {
				temp.put(text1, 1.0);
			} else {
				temp.put(text1, temp.get(text1) + 1);
			}
		}

		double sum = 0;

		for (HashMap.Entry<String, Double> entry : temp.entrySet()) {
			if (!index2.containsKey(entry.getKey())) {
				entry.setValue(0.0);
			} else {
				entry.setValue((1 + Math.log10(entry.getValue())) * index2.get(entry.getKey()));
				sum += entry.getValue() * entry.getValue();
			}
		}

		sum = Math.sqrt(sum);

		for (HashMap.Entry<String, Double> entry : temp.entrySet()) {
			entry.setValue(entry.getValue() / sum);
		}

		for (String text1 : text) {
			if (!index.containsKey(text1)) {
				continue;
			}
			for (HashMap.Entry<Integer, Double> entry : index.get(text1).getInfo().entrySet()) {
				if (score.containsKey(entry.getKey())) {
					score.put(entry.getKey(), score.get(entry.getKey()) + (entry.getValue() * temp.get(text1)));
				} else {
					score.put(entry.getKey(), entry.getValue() * temp.get(text1));
				}
			}
		}
	}

}