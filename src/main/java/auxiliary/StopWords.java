package auxiliary;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * 
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class StopWords {
	
	protected static ArrayList<String> stopWords = new ArrayList<>();

	private static final File ROOT = new File("files");
	private static final String BARRA = System.getProperty("file.separator");
	private static final String STOP = ROOT + BARRA + "stop.txt";

	/**
	 * Le stopwords do documento txt do path recebido 
	 * @param path
	 * @throws FileNotFoundException
	 */
	public static void lerStopWords() throws FileNotFoundException {
		Scanner sc = new Scanner(new File(STOP));

		//guardar stopwords no arraylist
		while (sc.hasNextLine()) {
			stopWords.add(sc.nextLine());
		}

		sc.close();
	}
	
}
