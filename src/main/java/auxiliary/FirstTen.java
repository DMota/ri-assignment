package auxiliary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class FirstTen {
	
	public static ArrayList<Integer> get10(HashMap<Integer, Double> ten){
		List<Map.Entry<Integer, Double>> entradas = new ArrayList<Entry<Integer, Double>>(ten.entrySet());
		entradas.sort((o1, o2) -> o2.getValue().compareTo(o1.getValue()));
		ArrayList<Integer> list10 = new ArrayList<>();
		
		for(int i = 0; i < 10; i++){
			list10.add(entradas.get(i).getKey());
		}
		
		return list10;		
	}

}
