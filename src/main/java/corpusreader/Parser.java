
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corpusreader;

import Assignment3.HeapMemory;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import tokenizer.Tokenizer;

/** 
 * 
 * @author André Moreira 62058 Diogo Mota 94956
 */
public class Parser {
	
	private static final File ROOT = new File("files");
	private static final String BARRA = System.getProperty("file.separator");
	private static final String CORPUS = ROOT + BARRA + "_corpus.tsv";
    
    private String texto;
    private String[] split_criteria;
    private StringBuilder sb;
    private int product_title_count, review_headline_count, review_body_count, word_match_count, i;
    private Tokenizer tok = new Tokenizer();

    public Parser() {
        sb = new StringBuilder();
        product_title_count = 0; review_headline_count = 0; review_body_count = 0; word_match_count = 0; i = 0;
        
//        this.parsingHeadline(sc);
//        WriteFile
    }
    
    /**
     *
     * @param sc
     * @return
     */
//    protected StringBuilder parsingHeadline(BufferedReader br) throws IOException{
    protected void parsingHeadline(BufferedReader br) throws IOException{
        String[] match_words = {"product_title", "review_headline", "review_body"};
        
        this.texto = br.readLine();
        this.split_criteria = this.texto.split("\t");
        
        for (String t : this.split_criteria){
            if (!t.equals(match_words[i])){
                switch (word_match_count){
                    case 0: 
                            product_title_count++;
                            break;
                    case 1:
                            review_headline_count++;
                            break;
                    case 2: 
                            review_body_count++;
                            break;
                    default:
                            break;
                }
            } else {
                word_match_count++;
                if (word_match_count != match_words.length) {i++; /*sb.append(t).append("\t");*/} else {/*sb.append(t).append("\n");*/ break;}
//                if (word_match_count == match_words.length) {break;}                
            }
        }
//        return sb;      
    }
    
    /**
     *
     * @param sc
     * @return
     * @throws IOException
     */
    protected StringBuilder parsingBody(BufferedReader br) throws IOException{
        HeapMemory hm = new HeapMemory();

        corpusreader.WriteFile wf = new WriteFile(CORPUS);
        sb.setLength(0);
//        int i = 0;
        int split_criteria_count, word_match;
        while ((this.texto = br.readLine()) != null){
//            i++;
//            System.out.print("number: " + i + " Parser: " + sb);
//            System.out.println("number: " + i);
            if (hm.getUsedHeapPercentage()>70d) { System.out.println(hm.setDecimalFormat(hm.getUsedHeapPercentage())+"%");/*i=0;*/ wf.fileBodyWrite(sb); sb.setLength(0); hm.freeHeapMemory(); }
//            texto = sc.nextLine();
            this.split_criteria = this.texto.split("\t");
            word_match = 0; split_criteria_count = 0;
        
            for (String t : this.split_criteria){
                switch (word_match) {
                    case 0:
                        if (product_title_count == split_criteria_count){
                            word_match++;
                            split_criteria_count = 0;
                            sb.append(t).append("\t");
                        } else {
                            split_criteria_count++;
                        }
                        break;
                    case 1:
                        if (review_headline_count == split_criteria_count){
                            word_match++;
                            split_criteria_count = 0;
                            sb.append(t).append("\t");
                        } else {
                            split_criteria_count++;
                        }
                        break;
                    case 2:
                        if (review_body_count == split_criteria_count){
                            word_match++;
                            sb.append(t).append("\n");
                        } else {
                            split_criteria_count++;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        
        wf.fileBodyWrite(sb);
        wf.closeFile();
//        wf.pw.append(sb);
//        wf.pw.close();
//        wf.fos.close();
//        wf.bw.close();
        return sb;
    }
}
