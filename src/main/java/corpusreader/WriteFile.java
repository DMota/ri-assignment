/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corpusreader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author André Moreira 62058 Diogo Mota 94956
 */
public class WriteFile {

    protected PrintWriter pw;
//    protected BufferedWriter bw;
    
    //teste
//    protected FileOutputStream fos;
    
    /**
     *
     * @param s
     * @throws IOException
     */
    public WriteFile(String s) throws IOException{
        String outputFile = s;
        this.pw = new PrintWriter(new FileWriter(outputFile));
//        this.bw = new BufferedWriter(new FileWriter(outputFile));
        
        //test
//        this.fos = new FileOutputStream(new File(outputFile));
    }
    
    /**
     *
     * @param sb
     * @throws IOException
     */
    protected void fileHeadWrite(StringBuilder sb) throws IOException{
//        System.out.println(sb);
//        this.pw.print(sb);
//        this.fos.write(sb.toString().getBytes());
//        this.pw.close();
//        this.bw.(sb);
//        this.bw.append(sb.toString());
    }
    
    /**
     *
     * @param sb
     * @throws IOException
     */
    public void fileBodyWrite(StringBuilder sb) throws IOException{
//        PrintWriter pw = new PrintWriter(new FileWriter("_new.tsv"));
        this.pw.append(sb);
//        this.fos.write(sb.toString().getBytes());
//        this.fos.close();
//        this.pw.close();
//        this.bw.append(sb.toString());
    }
    
    public void closeFile() throws IOException{
//        this.fos.close();
        this.pw.close();
    }
    
    
    
}