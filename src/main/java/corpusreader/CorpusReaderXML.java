/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corpusreader;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author André Moreira 62058 Diogo Mota 94956
 */
public class CorpusReaderXML {
    
    private static final File ROOT = new File("files");
    private static final String BARRA = System.getProperty("file.separator");
    private static final String CRANFIELD = ROOT + BARRA + "cranfield";
    private static final String OUTPUTFILE = ROOT + BARRA + "_XMLcorpus.tsv";
        
    public StringBuilder corpusXML() throws IOException{
        ReadFile rf = new ReadFile();
        ParserXML parse = new ParserXML();
        File[] listOfFiles = rf.getFiles(CRANFIELD);
        
        StringBuilder sb = new StringBuilder();
        for (File file: listOfFiles){
            sb.append(parse.parseXML(file));
        }
        WriteFile wf = new WriteFile(OUTPUTFILE);
        wf.fileBodyWrite(sb);
        wf.closeFile();
        return sb;
    }
}
