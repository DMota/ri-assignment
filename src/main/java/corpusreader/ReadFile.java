/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corpusreader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;
import org.xml.sax.SAXException;

/**
 *
 * @author André Moreira 62058 Diogo Mota 94956
 */
public class ReadFile {
    
    private File ficheiro;

    
    /**
     *
     */
    public ReadFile(){
//        this.ficheiro = new File(nomeFicheiro);
    }
    
    public File getNomeFicheiro(){
        return this.ficheiro;
    }
    
    public File setNomeFicFile(){
        return this.ficheiro;
    }
    
    /**
     *
     * @param ficheiro
     * @return
     * @throws FileNotFoundException
     */
    public static BufferedReader readFile(File ficheiro) throws FileNotFoundException{
        FileReader fr = new FileReader(ficheiro);
        BufferedReader br = new BufferedReader(fr);
//        Scanner sc = new Scanner(br);
        return br;
        
    }
    
    public static File[] getFiles(String directory) throws IOException{
        File folder = new File(directory);
        File[] listOfFiles = folder.listFiles();
        
        return listOfFiles;
    }

}
