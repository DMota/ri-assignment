/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corpusreader;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author André Moreira 62058 Diogo Mota 94956
 */
public class CorpusReader {
    
    public void corpusReader(){
        
    }
    
    public Parser fileReader(File ficheiro) throws FileNotFoundException, IOException {
        ReadFile rf = new ReadFile();
        BufferedReader br = rf.readFile(ficheiro);
        
        Parser parsing = new Parser();
        parsing.parsingHeadline(br);
        
//        System.out.println(parsing.parsingHeadline(sc));
//        writeFile wf = new writeFile();
//        wf.fileHeadWrite(parsing.parsingHeadline(sc));
        
        parsing.parsingBody(br);
//        System.out.println(parsing.parsingBody(sc));

        return parsing;
    }
    
    
    /*
    public void fileReader_original (File ficheiro) throws FileNotFoundException, IOException{
        String outputFile = "_new.tsv";
        PrintWriter pw = new PrintWriter(new FileWriter(outputFile));
        
        FileReader fr = new FileReader(ficheiro);
        Scanner sc = new Scanner(fr);
        String texto;
        String[] match_word = {"product_title", "review_headline", "review_body"};
        int product_title_count = 0, review_headline_count = 0, review_body_count = 0, word_match_count = 0, i = 0;
        StringBuilder sb = new StringBuilder();
        
        texto = sc.nextLine();
        String[] token = texto.split("\t");
        
//       do{
            for (String t : token){
                if (!t.equals(match_word[i])){
    //                texto = sc.nextLine();
//                    if (word_match_count <= 3) product_title_count++;
                    switch (word_match_count){
                        case 0: 
                                product_title_count++;
                                break;
                        case 1:
                                review_headline_count++;
                                break;
                        case 2: 
                                review_body_count++;
                                break;
                        default:
                                break;
                    }
//                    System.out.println(t + " ASDASDA = " + product_title_count + " ASDASD = " + review_headline_count + " ASDSADAS = " + review_body_count);
                } else {
                    word_match_count++;
//                    System.out.println(t + " ASDASDA = " + product_title_count + " ASDASD = " + review_headline_count + " ASDSADAS = " + review_body_count);
//                    System.out.println(word_match_count);
                    if (word_match_count != 3) {i++; sb.append(t).append("\t");} else {sb.append(t).append("\n"); break;}
//                    break;
                }
//                break;
            }
//        } while (i < match_word.length);

        int token_count, token_match;
        while (sc.hasNextLine()){
            System.out.print(sb);
            pw.print(sb);
            sb.setLength(0);
            texto = sc.nextLine();
            token = texto.split("\t");
            token_match = 0; token_count = 0;
//            String[] token = texto.split("\t");
        
            for (String t : token){
//                System.out.println(t);
    //                texto = sc.nextLine();
                    switch (token_match) {
                        case 0:
                            if (product_title_count == token_count){
                                token_match++;
                                token_count = 0;
                                sb.append(t).append("\t");
//                                System.out.print(t + " : ");
                            } else {
                                token_count++;
                            }
                            break;
                        case 1:
                            if (review_headline_count == token_count){
                                token_match++;
                                token_count = 0;
                                sb.append(t).append("\t");
//                                System.out.print(t + " : ");
                            } else {
                                token_count++;
                            }
                            break;
                        case 2:
                            if (review_body_count == token_count){
                                token_match++;
                                sb.append(t).append("\n");
//                                System.out.println(t);                                
                            } else {
                                token_count++;
                            }
                            break;
                        default:
                            break;
                    }
//                    break;
//                    token_count = 0;
//                    break;
//                } else {
//                    token_count++;

//                    System.out.println(t + " ASDASDA = " + product_title_count + " ASDASD = " + review_headline_count + " ASDSADAS = " + review_body_count);
//                    System.out.println(word_match_count);
//                    if (word_match_count != 3) i++; else break;
//                    break;
                }
            }
        
            pw.close();
        }
*/
}
