/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corpusreader;

import java.io.File;
import java.io.FileNotFoundException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author André Moreira 62058 Diogo Mota 94956
 */
public class ParserXML {

    public static StringBuilder parseXML(File ficheiro) throws FileNotFoundException{
        StringBuilder sb = new StringBuilder();
        try{
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(ficheiro);
            String title = document.getElementsByTagName("TITLE").item(0).getTextContent();
            String text = document.getElementsByTagName("TEXT").item(0).getTextContent();
            sb.append(title+"\t"+text);
//            sb.append(title+"\t"+text+"\n");
//            System.out.println(title + "\n-----------\n"+text+"\n###################");

        } catch (Exception e){
            e.printStackTrace();
        }

        return sb;
    }
    
}
