package Assignment3;

import corpusreader.CorpusReader;
import corpusreader.CorpusReaderXML;
import corpusreader.ParserXML;
import corpusreader.ReadFile;
import corpusreader.WriteFile;
import tokenizer.Tokenizer;
import indexer.Indexer;
import query.Queries;
import rankedretrieval.RankedRetrieval;
import relevance.Relevance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Timer;

import auxiliary.Data;
import auxiliary.Metrics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author André Moreira 62058 Diogo Mota 94956
 */
public class Assignment3 {

	private static final File ROOT = new File("files");
	private static final String BARRA = System.getProperty("file.separator");
	private static final String FILE = ROOT + BARRA + "amazon_reviews_us_Watches_v1_00.tsv";
	private static final String CRANFIELD = ROOT + BARRA + "cranfield";

	/**
	 *
	 */
	public Assignment3() {

	}

	/**
	 * Classe main assignment RI
	 * @param args
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException {

		//File ficheiro = new File(FILE);
		HeapMemory hm = new HeapMemory();
		//System.out.println(hm.getHeapSize());

		CorpusReaderXML cr = new CorpusReaderXML();
		StringBuilder corpus = cr.corpusXML();
		//corpus.append(cr.fileReader(ficheiro));
		//System.gc();
		//System.out.println(hm.getUsedHeapPercentage());
		System.out.println("Acabou de processar o Corpus!");

		// Tokenizer
		corpus.setLength(0);        
		hm.freeHeapMemory();
		Tokenizer tok = new Tokenizer();
		tok.tokenize();
		System.out.println("Acabou de processar o Tokenizer!");

		//Indexer
		hm.freeHeapMemory();
		Indexer index = new Indexer();
		index.indexar();
		System.out.println("Acabou de processar o Indexer!");

		//Extended Indexer
		hm.freeHeapMemory();
		//long startTime = System.currentTimeMillis();
		index.extendIndexar();
		//long endTime = System.currentTimeMillis();
		//long duration = (endTime - startTime)/1000;
		System.out.println("Acabou de processar o Extended Indexer! " /*+ duration + " segundos"*/);

		//RankedRetrieval
		hm.freeHeapMemory();
		RankedRetrieval rank = new RankedRetrieval();
		//long startTime2 = System.currentTimeMillis();
		rank.rankRet();
		//long endTime2 = System.currentTimeMillis();
		//long duration2 = (endTime2 - startTime2)/1000;
		System.out.println("Acabou de processar o Ranked Retrieval! " /*+ duration2 + " segundos"*/);

		
		//Queries
		hm.freeHeapMemory();
		long start = System.currentTimeMillis();
		Queries queries = new Queries();
		queries.processarQueries();
		System.out.println("Acabou de processar as Queries!");
		
		//Relevance
		hm.freeHeapMemory();
		Relevance rel = new Relevance();
		HashMap<Integer, Data> relScore = rel.readRelevance();
		long end = System.currentTimeMillis();
		System.out.println("Acabou de processar os Relevance Scores!");
		
		//Metricas
		Metrics metrics = new Metrics(relScore);
		System.out.println("--------------------------------------------------------------------");
		System.out.println("Metricas:");
		System.out.println("--------------------------------------------------------------------");
		System.out.format("Precision: %.3f\n", metrics.getMeanPrecision());
		System.out.format("Recall: %.3f\n", metrics.getMeanRecall());
		System.out.format("F-Measure: %.3f\n", metrics.getMeanFMeasure());
		System.out.format("Mean Average Precision: %.3f\n", metrics.getMeanAveragePrecision());
		System.out.format("Mean Precision at Rank 10: %.3f\n", metrics.getMeanPrecisionAtRank10());
		System.out.format("Mean Reciprocal Rank: %.3f\n", metrics.getNDCG());
		double latency = ((double) (end - start) / queries.size()) / 1000;
		System.out.format("Mean Latency: %.3f second/query\n", latency);
		System.out.format("Query Throughput: %d queries/second\n", Math.round(1 / latency));

	}
}
