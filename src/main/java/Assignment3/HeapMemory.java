/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Assignment3;

import java.text.DecimalFormat;

/**
 *
 * @author André Moreira 62058 Diogo Mota 94956
 */
public class HeapMemory {
    
    public HeapMemory(){
        // Get current size of heap in bytes
        long heapSize = Runtime.getRuntime().totalMemory(); 

        // Get maximum size of heap in bytes. The heap cannot grow beyond this size.// Any attempt will result in an OutOfMemoryException.
        long heapMaxSize = Runtime.getRuntime().maxMemory();

         // Get amount of free memory within the heap in bytes. This size will increase // after garbage collection and decrease as new objects are created.
        long heapFreeSize = Runtime.getRuntime().freeMemory();
    }
    
    public String setDecimalFormat(double number){
        // Define decimal format
        DecimalFormat df = new DecimalFormat("#.00");
        return df.format(number);
    }
    
    /**
     * Get current size of heap in bytes
     * @return current size of heap in bytes
     */
    public long getHeapSize(){
        return Runtime.getRuntime().totalMemory();
    }
    
    /**
     * Get maximum size of heap in bytes. The heap cannot grow beyond this size.
     * Any attempt will result in an OutOfMemoryException.
     * @return maximum size of heap in bytes
     */
    public long getMaxSize(){
        
        return Runtime.getRuntime().maxMemory();
    }
    
    /**
     * Get amount of free memory within the heap in bytes. 
     * This size will increase after garbage collection and decrease as new objects are created.
     * @return amount of free memory within the heap in bytes
     */
    public long getFreeSize(){
        return Runtime.getRuntime().freeMemory();
    }
    
    /**
     * Get percentage of current size of heap
     * @return percentage of current size of heap
     */
    public double getUsedHeapPercentage(){
//        double usedHeapMemory = (double)getHeapSize()/(double)getMaxSize();
        double usedHeapMemory = (double)getHeapSize() - (double)getFreeSize();
        usedHeapMemory = usedHeapMemory / (double) getHeapSize();
        usedHeapMemory *= 100;
//        System.out.println(setDecimalFormat(usedHeapMemory) + "%"); 
        return usedHeapMemory;
    }
    
    /**
     * Get percentage of current free memory within the heap
     * @return percentage of current free memory within the heap
     */
    public float getFreeHeapPercentage(){
        float freeHeapMemory = getFreeSize()/getMaxSize();
        freeHeapMemory *= 100;
//        System.out.println(setDecimalFormat(freeHeapMemory) + "%"); 
        return freeHeapMemory;
    }
    
    public void freeHeapMemory(){
        System.gc();
//        System.out.println(getUsedHeapPercentage());
    }
}
