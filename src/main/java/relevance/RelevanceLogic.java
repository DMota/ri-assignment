package relevance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import auxiliary.Data;

/**
 * 
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class RelevanceLogic {
	
	/**
	 * Construtor
	 */
	public RelevanceLogic() {

	}

	/**
	 * 
	 * @param relevanceScores
	 * @param relevance
	 * @return 
	 * @return 
	 * @throws IOException
	 */
	public HashMap<Integer, Data> readRel(HashMap<Integer, Data> relevanceScores, String relevance) throws IOException {
		//System.out.println("Entrei logica relevance.");
        FileReader fr = new FileReader(relevance);
        BufferedReader br = new BufferedReader(fr);
        String line;
        while ((line = br.readLine()) != null) {
            String[] t = line.split("\\s+");
            Data d = new Data();
            for (int i = 1; i < t.length; i++) {
                if (relevanceScores.containsKey(Integer.parseInt(t[0]))) {
                    relevanceScores.get(Integer.parseInt(t[0])).getInfo().put(Integer.parseInt(t[1]), Double.parseDouble(t[2]));
                } else {
                    d.getInfo().put(Integer.parseInt(t[1]), Double.parseDouble(t[2]));
                    relevanceScores.put(Integer.parseInt(t[0]), d);
                }
            }
        }
        return relevanceScores;
}
}
