package relevance;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import auxiliary.Data;

/**
 * 
 * @author André Moreira 62058 Diogo Mota 94956
 *
 */
public class Relevance {

	private static RelevanceLogic rLogic = new RelevanceLogic();
	
	private static final File ROOT = new File("files");
	private static final String BARRA = System.getProperty("file.separator");
	private static final String SCORES = ROOT + BARRA + "cranfield.query.relevance.txt";
	
	private HashMap<Integer, Data> relevanceScores;
	
	/**
	 * Construtor
	 */
	public Relevance(){
		this.relevanceScores = new HashMap<>();
	}
	
	/**
	 * @return 
	 * @throws IOException 
	 * 
	 */
	public HashMap<Integer, Data> readRelevance() throws IOException {
		return rLogic.readRel(relevanceScores, SCORES);
	}
	
}
